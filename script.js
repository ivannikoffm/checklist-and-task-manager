document.querySelector('#addtask').addEventListener('keydown', (e) => {
    if (e.code == 'Enter' && e.currentTarget.value.length != 0) {
        let newTaskNode = createTaskDiv(e.currentTarget.value);
        document.querySelector('#tasks').style.visibility = 'visible';
        document.querySelector('#tasks').insertBefore(newTaskNode, document.querySelector('#tasks').firstElementChild);
        document.querySelector('#addtask').value = '';
    }
});
// Create Task-container HTML Node
function createTaskDiv(taskString) {
    //Create div task container
    let mainDiv = document.createElement('div');
    mainDiv.classList.add('task');

    //Create checkbox
    let checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.addEventListener('change', completeTask);

    //Create div task string
    let divtaskString = document.createElement('div');
    divtaskString.innerText = taskString;
    divtaskString.classList.add('taskstring');
    divtaskString.addEventListener('dblclick', taskStringOndblClick);

    //Create div close
    let divclose = document.createElement('div');
    divclose.classList.add('close');
    divclose.addEventListener('click', () => { event.currentTarget.parentNode.remove(); checkAllTasksDone()});
    
    //Merge all elements
    mainDiv.appendChild(checkbox);
    mainDiv.appendChild(divtaskString);
    mainDiv.appendChild(divclose);
    return mainDiv;
}

function completeTask(event) {
    event.currentTarget.parentElement.querySelector('.taskstring').style.textDecoration = 'line-through';
    event.currentTarget.remove();
}

// If all tasks are done - disable wrapper block and its borders
function checkAllTasksDone() {
    if (document.querySelector('#tasks').children.length < 1) 
        document.querySelector('#tasks').style.visibility = 'hidden';
}

function taskStringOndblClick(event) {
    
    let val = event.currentTarget.innerText;
    let tempInput = document.createElement('input');
    tempInput.type = 'text';
    tempInput.value = val;
    event.currentTarget.innerText = '';
    event.currentTarget.appendChild(tempInput);

    tempInput.addEventListener('change', (eve) => {
        eve.currentTarget.parentElement.innerText = eve.currentTarget.value;
        eve.currentTarget.remove();
    });
}